<?php

namespace App\Enums;

/**
 * Class EntityStatus
 * @package App\Enums
 */
class RoleEnum extends AbstractEnum
{
    /**
     * @var string
     */
    const USER_ROLE = 'user';

    /**
     * @var string
     */
    const ADMIN_ROLE = 'admin';

    /**
     * @var array
     */
    protected static $labels = [
        self::USER_ROLE => 'enums/role.user',
        self::ADMIN_ROLE => 'enums/role.admin'
    ];
}
