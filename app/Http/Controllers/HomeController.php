<?php
namespace App\Http\Controllers;

use App\Models\User;
use App\Models\UserGroup;

class HomeController extends Controller
{
    public function __invoke()
    {
        $usersCount = User::count();
        $groupsCount = UserGroup::count();

        return view('home', compact('usersCount', 'groupsCount'));
    }
}