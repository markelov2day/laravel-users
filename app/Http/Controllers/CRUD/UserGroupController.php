<?php

namespace App\Http\Controllers\CRUD;

use App\Enums\RoleEnum;
use App\Http\Controllers\Controller;
use App\Models\UserGroup;
use App\Http\Requests\StoreUserGroupRequest;

class UserGroupController extends Controller
{
    public function index()
    {
        $userGroups = UserGroup::get();

        return view('crud.user_groups.index', compact('userGroups'));
    }

    public function create()
    {
        return view('crud.user_groups.create', [
            'rolesSelectData' => RoleEnum::labels()
        ]);
    }

    public function store(StoreUserGroupRequest $request)
    {
        UserGroup::create($request->only('name', 'description', 'role'));

        return redirect()
            ->route('crud.user-groups.index');
    }

    public function edit($id)
    {
        return view('crud.user_groups.edit', [
            'userGroup' => UserGroup::find($id),
            'rolesSelectData' => RoleEnum::labels()
        ]);
    }

    public function update(StoreUserGroupRequest $request, $id)
    {
        UserGroup::findOrFail($id)->update($request->only('name', 'description', 'role'));

        return redirect()
            ->route('crud.user-groups.index');
    }

    public function destroy($id)
    {
        UserGroup::find($id)->delete();
    }
}
