<?php

namespace App\Http\Controllers\CRUD;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreUserRequest;
use App\Models\User;
use App\Models\UserGroup;

class UserController extends Controller
{
    public function index()
    {
        $users = User::with('group')->get();

        return view('crud.users.index', compact('users'));
    }

    public function create()
    {
        return view('crud.users.create', [
            'groupsSelectData' => UserGroup::getSelectData()
        ]);
    }

    public function store(StoreUserRequest $request)
    {
        User::create($request->only('first_name', 'last_name', 'phone', 'email', 'user_group_id'));

        return redirect()
            ->route('crud.users.index');
    }

    public function edit($id)
    {
        return view('crud.users.edit', [
            'user' => User::find($id),
            'groupsSelectData' => UserGroup::getSelectData()
        ]);
    }

    public function update(StoreUserRequest $request, $id)
    {
        User::findOrFail($id)->update($request->only('first_name', 'last_name', 'phone', 'email', 'user_group_id'));

        return redirect()
            ->route('crud.users.index');
    }

    public function destroy($id)
    {
        User::find($id)->delete();
    }
}
