<?php

namespace App\Models;

use App\Enums\RoleEnum;
use Illuminate\Database\Eloquent\Model;

/**
 * Class UserGroup
 * @package App\Models
 * @property int $id
 * @property string $name
 * @property string $description
 * @property string $role
 * @property Carbon\Carbon $created_at
 * @property Carbon\Carbon $updated_at
 */
class UserGroup extends Model
{
    protected $guarded = ['id'];

    protected $appends = ['roleName', 'editUrl', 'deleteUrl'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function users()
    {
        return $this->hasMany('users');
    }

    /**
     * @return array
     */
    public static function getSelectData()
    {
        $selectOptions = [];
        $items = self::get();
        $items->each(function ($item) use (&$selectOptions) {
            $selectOptions[$item->id] = $item->name;
        });
        return $selectOptions;
    }

    /**
     * @return string
     */
    public function getRoleNameAttribute()
    {
        return RoleEnum::label($this->role);
    }

    /**
     * @return string
     */
    public function getEditUrlAttribute()
    {
        return route('crud.user-groups.edit', ['user_group' => $this->id]);
    }

    /**
     * @return string
     */
    public function getDeleteUrlAttribute()
    {
        return route('crud.user-groups.destroy', ['user_group' => $this->id]);
    }
}
