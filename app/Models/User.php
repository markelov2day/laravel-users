<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class User
 * @package App\Models
 * @property int $id
 * @property string $first_name
 * @property string $last_name
 * @property string $phone
 * @property string $email
 * @property Carbon\Carbon $created_at
 * @property Carbon\Carbon $updated_at
 */
class User extends Model
{
    protected $guarded = ['id'];

    protected $appends = ['groupName', 'editUrl', 'deleteUrl'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function group()
    {
        return $this->belongsTo(UserGroup::class, 'user_group_id', 'id');
    }

    /**
     * @return string
     */
    public function getGroupNameAttribute()
    {
        return $this->group->name;
    }

    /**
     * @return string
     */
    public function getEditUrlAttribute()
    {
        return route('crud.users.edit', ['user' => $this->id]);
    }

    /**
     * @return string
     */
    public function getDeleteUrlAttribute()
    {
        return route('crud.users.destroy', ['user' => $this->id]);
    }
}
