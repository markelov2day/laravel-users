@extends('layouts.master')

@section('title') Create user group @endsection

@section('content')
    <div id="app">
        <h1 class="page-header text-center">
            <i class="fa fa-pencil"></i> Create user group
        </h1>
        <div class="row">
            @if (count($errors) > 0)
                @include('layouts.partials.validation_errors')
            @endif
            {{ Form::open(['url' => route('crud.user-groups.store')]) }}
            <div class="form-group">
                <label for="name">Name</label>
                {{ Form::text('name', old('name'), ['class' => 'form-control']) }}
            </div>
            <div class="form-group">
                <label for="description">Description</label>
                {{ Form::text('description', old('description'), ['class' => 'form-control']) }}
            </div>
            <div class="form-group">
                <label for="role">Role</label>
                {{ Form::select('role', $rolesSelectData, null, ['class' => 'form-control']) }}
            </div>
            <div class="button-group">
                <a href="{{ route('crud.user-groups.index') }}" class="btn btn-default">
                    <i class="fa fa-arrow-left"></i> Back
                </a>
                {{ Form::submit('Save', ['class' => 'btn btn-success']) }}
            </div>
            {{ Form::close() }}

        </div>
    </div>
@endsection
