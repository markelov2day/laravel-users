@extends('layouts.master')

@section('title') Edit user @endsection

@section('content')
    <div id="app">
        <h1 class="page-header text-center">
            <i class="fa fa-pencil"></i> Edit user #{{ $user->id }}
        </h1>
        <div class="row">
            @if (count($errors) > 0)
                @include('layouts.partials.validation_errors')
            @endif
            {{ Form::open(['url' => route('crud.users.update', ['user' => $user->id]), 'method' => 'put']) }}
                <div class="form-group">
                    <label for="first_name">First name</label>
                    {{ Form::text('first_name', old('first_name') ?? $user->first_name, ['class' => 'form-control']) }}
                </div>
                <div class="form-group">
                    <label for="last_name">Last name</label>
                    {{ Form::text('last_name', old('last_name') ?? $user->last_name, ['class' => 'form-control']) }}
                </div>
                <div class="form-group">
                    <label for="phone">Phone</label>
                    {{ Form::text('phone', old('phone') ?? $user->phone, ['class' => 'form-control']) }}
                </div>
                <div class="form-group">
                    <label for="first_name">Email</label>
                    {{ Form::email('email', old('email') ?? $user->email, ['class' => 'form-control']) }}
                </div>
                <div class="form-group">
                    <label for="user_group_id">User group</label>
                    {{ Form::select('user_group_id', $groupsSelectData, $user->user_group_id, ['class' => 'form-control']) }}
                </div>
                <div class="button-group">
                    <a href="{{ route('crud.users.index') }}" class="btn btn-default">
                        <i class="fa fa-arrow-left"></i> Back
                    </a>
                    {{ Form::submit('Save', ['class' => 'btn btn-success']) }}
                </div>
            {{ Form::close() }}

        </div>
    </div>
@endsection
