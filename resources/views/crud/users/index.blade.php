@extends('layouts.master')

@section('title') Users @endsection

@section('content')
    <div id="app">
        <h1 class="page-header text-center">
            <i class="fa fa-address-book"></i> Users
        </h1>
        <div class="row">
            <div class="col-xs-6">
                <div class="btn-group">
                    <a href="{{ route('crud.users.create') }}" class="btn btn-primary">
                        <i class="fa fa-plus"></i> Create New
                    </a>
                </div>
            </div>
            <div class="col-xs-6">
                <input v-model="query" type="text" class="form-control" placeholder="Search...">
            </div>
        </div>

        <div class="table-responsive">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th v-for="column in columns" v-on:click="sortBy(column.propertyName)" style="cursor: pointer">
                        @{{ column.displayName }}
                        <i v-if="sortKey == column.propertyName" v-bind:class="'fa '+[sortDirection == 'asc' ? 'fa-chevron-up' : 'fa-chevron-down']"></i>
                    </th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                <tr v-for="item in filter(items)">
                    <td v-for="column in columns">
                        @{{ item[column.propertyName] }}
                    </td>
                    <td>
                        <a v-bind:href="item.editUrl" class="btn btn-success">
                            <i class="fa fa-pencil"></i>
                        </a>
                        <a v-on:click.prevent="removeItem(item)" href="#" class="btn btn-danger">
                            <i class="fa fa-trash"></i>
                        </a>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('footer_scripts')
    <script>
        var appConfigs = {
            columns: [
                {propertyName: 'id', displayName: '#'},
                {propertyName: 'first_name', displayName: 'First Name'},
                {propertyName: 'last_name', displayName: 'Last Name'},
                {propertyName: 'groupName', displayName: 'Group Name'},
                {propertyName: 'phone', displayName: 'Phone'},
                {propertyName: 'email', displayName: 'Email'}
            ],
            items: JSON.parse('{!! $users->toJson() !!}')
        };
    </script>
    <script src="{{ asset('js/app.js') }}"></script>
@endsection