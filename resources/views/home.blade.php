@extends('layouts.master')

@section('title') User Management @endsection

@section('content')
    <h1 class="page-header text-center">User Management</h1>
    <div class="row placeholders">
        <div class="col-xs-6 placeholder">
            <a href="{{ route('crud.users.index') }}">
                <i class="fa fa-address-card fa-5x"></i>
                <h4>Users</h4>
                <span class="text-muted">{{ $usersCount }}</span>
            </a>
        </div>
        <div class="col-xs-6 placeholder">
            <a href="{{ route('crud.user-groups.index') }}">
                <i class="fa fa-users fa-5x"></i>
                <h4>User Groups</h4>
                <span class="text-muted">{{ $groupsCount }}</span>
            </a>
        </div>
    </div>
@stop
