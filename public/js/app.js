var app = new Vue({
    el: '#app',
    data: {
        columns: appConfigs.columns,
        query: '',
        sortKey: 'id',
        sortDirection: 'asc',
        items: appConfigs.items
    },
    methods: {
        filter: function () {
            var query = this.query;
            var columns = this.columns;
            return this.items.filter(function (item) {
                if (query == '') {
                    return true;
                }
                var result = false;
                columns.forEach(function (column) {
                    if (String(item[column.propertyName]).toLowerCase().indexOf(query.toLowerCase()) !== -1) {
                        result = true;
                    }
                });
                return result;
            });
        },
        sortBy: function (sortKey) {
            if (this.sortKey == sortKey) {
                this.sortDirection = this.sortDirection == 'asc' ? 'desc' : 'asc';
            }
            this.sortKey = sortKey;
            var sortDirection = this.sortDirection;
            this.items = this.items.sort(function compare(a,b) {
                if (a[sortKey] < b[sortKey])
                    return sortDirection == 'asc' ? -1 : 1;
                if (a[sortKey] > b[sortKey])
                    return sortDirection == 'asc' ? 1 : -1;
                return 0;
            });
        },
        removeItem: function (item) {
            var self = this;
            if (confirm('Do you really want to delete this item?')) {
                $.ajax({
                    url: item.deleteUrl,
                    type: 'DELETE',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    },
                    success: function() {
                        self.items.splice(self.items.indexOf(item), 1);
                    }
                });
            }
        }
    }
});
