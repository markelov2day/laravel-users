<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', ['uses' => 'HomeController']);

Route::group(['prefix' => 'crud', 'namespace' => 'CRUD', 'as' => 'crud.'], function () {
    Route::resource('users', 'UserController', ['except' => 'show']);
    Route::resource('user-groups', 'UserGroupController', ['except' => 'show']);
});

